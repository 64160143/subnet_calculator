# -*- coding: utf-8 -*-
"""subnet_calculator.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1uHu2ob3JzB1vtjpsSkYc2GnJCf7mU7x-
"""

import ipaddress

def calculate_subnet():
    try:
        ip = ipaddress.IPv4Network(input("Enter IP address and prefix length (e.g., 192.168.1.0/24): "), strict=False)
        network_address = str(ip.network_address)
        broadcast_address = str(ip.broadcast_address)
        total_hosts = ip.num_addresses - 2

        print(f"Network Address: {network_address}")
        print(f"Broadcast Address: {broadcast_address}")
        print(f"Total Hosts: {total_hosts}")
    except ValueError:
        print("Invalid IP or Prefix Length")

if __name__  == "__main__":
    calculate_subnet()

!pip install ipaddress